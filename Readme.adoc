= Root in container

//tag::abstract[]

Running processes in the container as root can 
be the same as running root on the host. 

//end::abstract[]

The root in the container is the same as on host unless remapped with user
namespaces. It is only lightly restricted by, primarily, Linux namespaces,
capabilities, and cgroups.

//tag::lab[]

== Lab

Your objective in each lab is to pass the build.


=== Task 0

*Fork* and clone this repository.
Install `docker`, `docker-compose` and `make` on your system.

. Run unit tests: `make test`. 
. Build the program: `make build`.
. Run it: `make run`.
. Run security tests: `make securitytest`.

Note: The last test will fail. 


=== Task 1

Review `Dockerfile` and `docker-compose.yml` file. 
Why the content of `/etc/shadow` on the host 
can be read by the container?
Find out the security weakness.

Note: Avoid looking at tests and try to
spot the vulnerability on your own.

=== Task 2

The security weakness is running processes as root inside the container.
This allow a file owned by the root on the host to be
accessed by a container even thought the container runs by a low privileged
user.  Edit the `docker-compose.yml` to use a low privileged user.
This time when you run the container, you should get
a permission denied error.

Note: Do NOT remove the mount volumes in `docker-compose.yml` 

=== Task 3

Run security tests again. Make sure build pass.
If you stuck, move to the next task.

=== Task 4 
 
Check out the `patch` branch and review the changes. 
Run all tests and make sure everything pass. 


=== Task 5 
 
Merge the patch branch to master. 
Commit and push your changes. 
Does pipeline for your forked repository show that you have passed the build?   

(Note: you do NOT need to send a pull request)


== Bonus

. Can you think of another way to fix this issue without touching `docker-compose.yml`?
. Is there a way to fix this issue, in a situation that both `Dockerfile` or `docker-compose.yml`
are not editable?

//end:lab[]

//tag::references[]

== References

* https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#user
* https://medium.com/@mccode/processes-in-containers-should-not-run-as-root-2feae3f0df3b
* https://docs.docker.com/engine/security/userns-remap/

//end::references[]
